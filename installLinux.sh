#!/bin/bash
clear
path=$(pwd)
if ! [ -d $HOME/.HHSW/autoPicturePy/bin ]
then
    echo ''
    echo "Installiere benötigte Software..."
    echo ''
    sleep 1
    sudo apt install python3
    sudo apt install libnotify-bin
    sudo apt install python3-pip
    pip install --upgrade virtualenv
    python3 -m virtualenv ~/.HHSW/autoPicturePy
fi
source ~/.HHSW/autoPicturePy/bin/activate
cd src
rm -rf __pycache__
python3 install.py
rm -rf __pycache__
cd ..
echo "Beenden mit Enter"
read line