class format:
    def __init__(self, value):
        self.__value = str(value)

    def getValue(self):
        value = []
        formatString = self.__value.split(',')
        formatString = str(formatString).split("'")
        for i in formatString:
            if i == '["[(' or i == ')]"]' or i == '", " ' or i == "(" or i == ")":
                pass
            else:
                value.append(str(i))
        return value

    def getValueForRowCount(self):
        value = []
        for i in self.__value:
            if i == "(" or i == ")" or i == "[" or i == "]" or i == ",":
                pass
            else:
                value.append(str(i))
        return value
