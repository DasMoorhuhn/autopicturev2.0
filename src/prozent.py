import sys
class pp:
    def pro(self, G, W):
        if G == 0 and W == 0:
            return 100
        p = W/G
        p = round(p, 2)

        if p < 1:
            p = str(p).split(".")
            if p[1] == "1":
                return("10")
            elif p[1] == "2":
                return("20")
            elif p[1] == "3":
                return("30")
            elif p[1] == "4":
                return("40")
            elif p[1] == "5":
                return("50")
            elif p[1] == "6":
                return("60")
            elif p[1] == "7":
                return("70")
            elif p[1] == "8":
                return("80")
            elif p[1] == "9":
                return("90")
            else:
                return(str(p[1]))
        else:
            p = str(p).split(".")
            return("100")
