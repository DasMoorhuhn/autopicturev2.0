import os
import shutil
import socket
import getpass
import sqliteHandler as sql

version = "2.0.13"
date = "30.08.2021"
devState = False

os.chdir("..")
connection = sql.sql("database.sqlite")
os.chdir("src")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serverIP = socket.gethostbyname("heine-ol.ddnss.de")
serverPORT = 61999


clsCommand = "none"
operationSystem = "none"
installPath = "none"
pipInstallCommand = "none"
slash = "none"
user = getpass.getuser()

if os.name == "posix": #Linux
    operationSystem = "Linux"
    installPath = f"/home/{user}/.HHSW/autoPicturePy"
    homePath = f"/home/{user}"
    clsCommand = "clear"
    pipInstallCommand = "pip install --upgrade"
    slash = "/"

if os.name == "mac": #Apple
    operationSystem = "Mac"
    installPath = f"/home/{user}/.HHSW/autoPicturePy"
    homePath = f"/home/{user}"
    clsCommand = "clear"
    pipInstallCommand = "pip install --upgrade"
    slash = "/"

if os.name == "nt": #Windows
    operationSystem = "Windows"
    installPath = f"C:\\users\\{user}\\.HHSW\\autoPicturePy"
    homePath = f"C:\\users\\{user}"
    clsCommand = "cls"
    pipInstallCommand = "pip install --upgrade"
    slash = "\\"

if installPath == "none" or operationSystem == "none" or slash == "none" or clsCommand == "none" or pipInstallCommand == "none":
    print("Fehler beim Erkennen vom Betriebssystem")
    print(f"System '{os.name}' konnte nicht eingerichtet werden")
    exit(1)

def initInstall():
    os.system(clsCommand)
    print("")
    if devState:
        print("BETA")
    print(f"+--------------------------<|AutoPicture|>-------------------------------")
    print(f"|")
    print(f"+ Installation für {operationSystem}")
    print(f"+ App Version: v{version} vom {date}")
    print(f"|")
    print(f"+------------------------------------------------------------------------")
    print("")
    try:
        inp = str(input(f"Quelle: {homePath}{slash}"))
        quelle = f"{homePath}{slash}{inp}"
        inp = str(input(f"Ziel: {homePath}{slash}"))
        ziel = f"{homePath}{slash}{inp}"
    except:
        exit("\nFehlerhafte Eingabe\n")
    print("")
    if devState:
        print("BETA")
    print(f"+-----------------------------<|Pfade|>----------------------------------")
    print(f"|")
    print(f"+ Installationspfad: {installPath}")
    print(f"+ Quelle: {quelle}")
    print(f"+ Ziel: {ziel}")
    print(f"|")
    print(f"+------------------------------------------------------------------------")
    print("")
    print("Sind diese Pfade korrekt?")
    print(" (1) Ja")
    print(" (2) Nein")
    print("")
    try:
        userIntput = str(input(">>> "))
        print("")
    except:
        exit("Fehlerhafte Eingabe")

    if userIntput == "1":     
        connection.install(installPath, homePath, user, operationSystem, quelle, ziel, version, date, slash, clsCommand)
        connection.close()
    elif userIntput == "2":
        initInstall()
    else:
        exit("\nFehlerhafte Eingabe\n")

def install():
    package = [ "pip", "pillow", "progressbar"]
    status = True
    try:
        for install in package:
            os.system(f"{pipInstallCommand} {install}")
    except:
        status = False
    os.chdir("..")
    try:
        shutil.copytree("src", f"{installPath}{slash}src")
        shutil.copyfile("database.sqlite", f"{installPath}{slash}database.sqlite")
    except:
        status = False
    if status == True:
        print("\nInstallation abgeschlossen\n")
        msg = f"autoPicture;install;{operationSystem}"
        if not devState:
            sock.sendto(msg.encode(), (serverIP, serverPORT))
    else:
        print("\nFehler beim Installieren\n")
            

def update():
    print("")
    if devState:
        print("BETA")
    print(f"+----------------------------<|Update|>----------------------------------")
    print(f"|")
    print(f"+ Von Version v{appVersion} zu v{version} vom {date}")
    print(f"|")
    print(f"+------------------------------------------------------------------------")
    print("")
    status = True
    try:
        shutil.rmtree(f"{installPath}{slash}src")
        os.chdir("..")
        shutil.copytree("src", f"{installPath}{slash}src")
    except:
        status = False
    con.update(version, date)
    if status == True:
        print("\nUpdate abgeschlossen\n")
        msg = f"autoPicture;update;{operationSystem};{appVersion};{version}"
        if not devState:
            sock.sendto(msg.encode(), (serverIP, serverPORT))
    else:
        print("\nFehler beim Installieren\n")




try:
    if os.path.exists(f"{installPath}{slash}src"):
        try:
            con = sql.sql(f"{installPath}{slash}database.sqlite")
        except:
            exit("Fehler beim Datenbanklesen")
        try:
            config = con.getConfig()
            appVersion = config[6]
            versFromOld = str(appVersion).split(".")
            versFromNew = str(version).split(".")
            if versFromOld[0] < versFromNew[0] or versFromOld[1] < versFromNew[1] or versFromOld[2] < versFromNew[2]:
                update()
            else:
                if versFromOld[0] == versFromNew[0] and versFromOld[1] == versFromNew[1] and versFromOld[2] == versFromNew[2]:
                    if devState:
                        update()
                    else:
                        print("Bereits aktuell")
                else:
                    update()
        except:
            exit("Unbekannter Fehler")
        finally:
            con.close()
    else:   
        initInstall()
        install()
        
finally:
    sock.close()
